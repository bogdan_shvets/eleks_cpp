#pragma once
class NumberRandomizer
{
private:
	int number = 0;


public:
	NumberRandomizer();
	~NumberRandomizer();

	bool checkNumber(int);
	void changeNumber();
};

